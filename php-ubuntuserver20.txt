https://distrowatch.com/dwres.php?resource=popularity
https://distrowatch.com/search.php?basedon=Ubuntu%20(LTS)#simple
nginx:1.18.0
mysql:10.3.22
php:7.4.3
nodejs:10.19.0
sudo -i
apt-get update && apt update
apt-get -y install net-tools git g++ php php7.4-dev composer php-mysql php-fpm php-bcmath php-curl php-bz2 php-gd php-imap php-mbstring php-pear php-xml php-xmlrpc php-zip nginx mariadb-server unzip

mysql_secure_installation

mysql -e "update mysql.user set authentication_string=password('root'), plugin = 'mysql_native_password' where user = 'root';flush privileges;"

git clone https://gitee.com/songzhenghe/environment.git /home/szh/environment --depth=1 && \
unzip /home/szh/environment/phpMyAdmin-4.9.5-all-languages.zip -d /var/www/html/ && \
mv -f /var/www/html/phpMyAdmin-4.9.5-all-languages /var/www/html/phpmyadmin

mv -f /etc/nginx/sites-enabled/default /home/szh/default && \
cp -f /home/szh/environment/nginx.conf-ubuntu-server-20 /etc/nginx/nginx.conf && \
cp -f /home/szh/environment/default-7.4 /etc/nginx/sites-enabled/default && \
cp -f /home/szh/environment/a.com-7.4 /etc/nginx/sites-enabled/a.com && \
cp -f /home/szh/environment/index.php /var/www/html/index.php && \
echo "127.0.0.1 a.com" >> /etc/hosts && \
service apache2 stop && \
apt-get -y remove apache2 && \
service nginx restart && \
service php7.4-fpm restart && \
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/

sudo apt-get -y install pure-ftpd &&
sudo pure-pw useradd szh -u szh -d /home/szh &&
sudo pure-pw mkdb &&
sudo service pure-ftpd restart &&
netstat -antl | grep 21

sudo apt-get install nodejs npm


Nginx出现413 Request Entity Too Large问题的解决方法
在http{}段中加入 client_max_body_size 200m;     200m为允许最大上传的大小。

sudo vim /etc/nginx/nginx.conf
client_max_body_size 200M;
sudo nginx -s reload 