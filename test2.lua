package.path = './?.lua;/home/szh/lua123/lua-resty-string/lib/?.lua;/usr/local/share/lua/5.3/?.lua'    --搜索lua模块


local openssl = require 'openssl'

 
-- 加密
local data = 'Hello, World!'

local cipher = 'aes-256-cbc'  -- 选择加密算法和模式
local key = 'gwTaLLxJUJjTx!xZ$&szBvPyyWh5aRUP'  -- 替换为你的密钥
local iv = key:sub(0, 16)    -- 替换为你的初始化向量

local encrypted = openssl.cipher.encrypt(cipher,data,key,iv,true,openssl.engine('pkcs7'))
print('Encrypted:', encrypted)
 
-- -- 解密
local decrypted = openssl.cipher.decrypt(cipher,encrypted,key,iv,false,openssl.engine('pkcs7'))
print('Decrypted:', decrypted)