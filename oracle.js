const oracledb = require('oracledb');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

const mypw = '1234abcd'  // set mypw to the hr schema password

async function run() {

    const connection = await oracledb.getConnection ({
        user          : "C##SZH",
        password      : mypw,
        connectString : "192.168.56.1:1521/XE"
    });

    const result = await connection.execute(`select 1+1 as sum from dual`);

    console.log(result);
    await connection.close();
}

run();
